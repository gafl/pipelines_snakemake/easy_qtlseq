To run this pipeline, you will need to prepare the following data:
- **sequencing data**: FASTQ files in compressed (``.gz``) format. You should create a repository that contains your sequencing data because the data headers may need to be modified to have the correct format (e.g., 2:N:0 for UMI sequence).To creat your data repository it, follow these command

```bash
mkdir [repository_name]
mkdir dataset
```
Now, you need to copie all these data in the 'repository_name' that you created. To do that, follow the command

```bash
cp path_to_the_data_file path_to_the_repository_destination
cp /work2/project/gafl/Data/Solanaceae/Solanum_lycopersicum/DNA/SPET/Invite/raw/DBC_AAOQOSDI_1_HVWC7DRXY.DUAL693_clean.fastq.gz /home/htphung/pipeline_qtlSeq/dataset 
```

- **reference genome data**: A FASTA-formatted file (``.fa`` or ``fasta``) of the reference genome for the organism of interest
- **sample file**: A CSV-formatted file (``.csv``) containing sample metadata and the corresponding names of the sequencing data files. Follow these [instructions](Generating-Sample-File) to generate a properly formatted sample file.

Please make sure to have all of the required data before running the QTLseq Analysis Pipeline. 
An example of a correct sample file is provided in the **samples.csv** file of the Git repository, which contains 4 columns:
- ``SampleName``: Specifies bulk names, including ``P1`` (for Parent 1), ``P2`` (for Parent 2), ``R`` (for Resistance), ``S`` (for Susceptible)
- ``mode``: Sequencing mode. The valid modes are: ``PE`` (Paired-End), ``SE`` (Single-End), ``SPET`` (Single Primer Enrichment Technology with UMI), and ``SPETNOUMI`` (Single Primer Enrichment Technology without UMI). Note that the pipeline is designed to handle different sequencing modes within a single run. It automatically detects the sequencing mode based on the provided sample files and adjusts its processing accordingly.
- ``fq1``: Forward reads (R1) sequencing data filename(s), which are required for all sequencing modes
- ``fq2``: Reverse reads (R2) or UMI sequencing data filename(s), which are only required for PE and SPET with UMI modes

| SampleName | mode | fq1 | fq2 |
| ---------- | ---- | --- | --- |
| P1 | spetnoumi | R1-P1.fastq.gz |
| P2 | pe | R1-P2.fastq.gz | R2-P2.fastq.gz |
| R | spet | R1-R.fastq.gz;R1-R_2.fastq.gz | R2-R.fastq.gz;R2-R_2.fastq.gz |
| S | spet | R1-S.fastq.gz;R1-S_2.fastq.gz | R2-S.fastq.gz;R2-S_2.fastq.gz |

You can use a text editor, Excel to create such a CSV file. 


Note that the QTLseq analysis pipeline is capable of processing multiple input files for each sample simultaneously, if needed. To specify multiple input files for a sample in the sample file, simply separate them with a semicolon (;) in the ``fq1`` and ``fq2`` column


_**IMPORTANT:**_ 
Please make sure the order of input files specified in the ``fq1`` column of the sample file need to corresponds correctly with the order of input files in the ``fq2``. If you are working with SE or SPET without UMI, the ``fq2`` column can be left empty.
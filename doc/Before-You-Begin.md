- Open a terminal window and navigate to the directory where you want to clone the Git repository.
- Run the following command to clone the Git repository:
```bash
git clone https://forgemia.inra.fr/gafl/users/ha_trang_phung/pipeline_qtlSeq.git
```
- Go to the work directory:
```bash
cd pipeline_qtlSeq
```
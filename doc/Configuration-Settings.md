The configuration file **config.yaml** contains the paths to the sample file, data directory, and reference genome, as well as various parameters for SNP calling and QTLseq R. It may also include container paths and parameters for binding Singularity containers for software dependencies.

To adapt the analysis, you may need to modify the values of the parameters specified in the config file. By changing these parameters, you can customize the behavior of the analysis pipeline according to your needs.

## 1. Data parameters
   - ``samplesfile``: path to the sample sheet file in CSV format.
    
   ```yaml
   samplesfile : "mysamplesfile.csv"
   ```
   - ``fq_dir``: path to the directory where the input fastq files are located.
   ```yaml
   fq_dir      : "/work2/project/gafl/Data/Solanaceae/Solanum_lycopersicum/DNA/SPET/DG-PHYTOM/raw"
   ```
   - ``outdir``: path to the directory where you want to store the output files generated by the analysis pipeline.
   ```yaml
   outdir      : "/work2/project/gafl/ReDD/DG-PHYTOM/hatrang/pipeline_qtlSeq/out"
   ```
   - ``GENOME``: name of the reference genome, which is typically a fasta or fa file with the extension.
   ```yaml
   GENOME      : "Sol_mic_1.0.fa"
   ```
   - ``REFPATH``: path to the directory where the reference genome file is located.
   ```yaml
   REFPATH     : "/work2/project/gafl/Data/Solanaceae/Solanum_lycopersicum/DNA/Ref_Genome/Microtom"
   ```

## 2. Singularity bind parameters and images location

Singularity bind parameters determine which host directories are mounted into the container for the analysis pipeline to access necessary software dependencies from the host system.

To work on different calculation servers, you need to modify the path of the singularity images location and also the singularity bind path to match your server's directory structure.

For example, to work on the **Genotoul** server, you would use:

```yaml
BIND : "-B /work/project/gafl"
fastqc_bin    : "/work/project/gafl/tools/containers/fastqc_V0.11.8.sif"
```

Otherwise, to work on the **GAFL** server, you would use:

```yaml
BIND : "-B /work2/project/gafl"
fastqc_bin    : "/work2/project/gafl/tools/containers/fastqc_V0.11.8.sif"
```

[COMPLETING] CI/CD




## 3. Remove PCR duplicates parameters

The variable ``bam_remove_duplicate`` is only needed when processing Paired-End data. To remove PCR duplicates after mapping reads, set the value to "y" for Yes. Otherwise, set it to "n" for No.

```yaml
bam_remove_duplicate : "y"
```

The ```probe_length``` variable is specifically used when processing SPET with UMI data. It represents the length of the probe (in bases) that needs to be trimmed after removing PCR duplicates to ensure the accuracy of the subsequent SNP calling step. 

```yaml
probe_length: 40
```

## 4. Select SNPs and INDELs parameters

In the context of selecting SNPs and INDELs for analysis, you can modify the following parameters:

``split_SNPs_INDELs``: This parameter controls whether to separate SNPs and INDELs into two different files or consider them all as variants in the analysis.
- If ``split_SNPs_INDELs``: ``y``, SNPs and INDELs will be separated into two files. Only the SNPs will be considered in the QTLseq analysis. The final output file, filtered.csv, will contain only the SNPs.

- If ``split_SNPs_INDELs``:``n``, SNPs and INDELs will not be separated, and both will be considered as variants in the QTLseq analysis. The final output file filtered.csv will contain both SNPs and INDELs. 
The variable ``max_INDEL_size``: This parameter allows you to specify the maximum size for INDELs to be included in the analysis. 

     ```yaml
     max_INDELs_size : 10 
     ``` 

For example INDELs larger than 10 bases will be filtered out. You can adjust this parameter according to your specific requirements.
   
By modifying these parameters, you can control how SNPs and INDELs are treated in the QTLseq analysis and customize the output accordingly.
## 5. GATK DP filtering parameters

Filtering SNPs by read depth helps to remove low-quality SNPs and increase the confidence in the final set of SNPs. Typically, you would want to filter SNPs based on the depth of coverage for the parents bulks and resistance/sensitivity bulks separately.

   - For the parents bulks, you can use ``dp_p`` to specify the minimum read depth. For example:

     ```yaml
     dp_p: 5
     ``` 
     it means that SNPs with a read depth lower than 5 will be filtered out.

   - For the F2 bulks, you can use ``min_dp`` et ``max_dp`` to specify the minimum et maximum read depth for each bulk. For example:

     ```yaml
     min_dp: 50
     max_dp: 1400
     ```
     it means that SNPs with a read depth lower than 50 or higher than 1400 will be filtered out.

It's important to note that the optimal read depth filter range may vary depending on the sequencing depth and quality of your data. Therefore, it's recommended to perform some testing and optimization to determine the appropriate filter range for your specific data set.

## 6. Statistical parameters

To prepare the data for analysis using QTLseqR, it is necessary to filter out low-confidence SNPs.

The ``min_depth_in_bulk`` and ``max_depth_in_bulk`` variables in the configuration file specify the minimum and maximum depth of reads allowed in each bulk for the SNP filtering step in the QTLseqR package. These thresholds can be applied to the total depth read (using ``minTotalDepth`` and ``maxTotalDepth``) in BOTH bulks and/or in each bulk separately (using ``minSampleDepth``):

```yaml
min_depth_in_bulk: 100 
max_depth_in_bulk: 1400
```
For [QTLseqR analysis](https://pubmed.ncbi.nlm.nih.gov/23289725/), you need to select the appropriate bulk size that you want to analyze. For example, 48 for both R and S bulk:

```yaml
R_bulk_size: 48
S_bulk_size: 48
```
For [G' analysis](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1002255), default parameters from the article are used, but they can be modified to improve the analysis.

```yaml
nb_takagi_reps: 10000
window_size: 1000000
filter_threshold: 0.1
false_discovery_rate_G: 0.1
```

You may refer to this [documentation](https://raw.githubusercontent.com/bmansfeld/QTLseqr/master/vignettes/QTLseqr.pdf) for more about the QTLseqr package.
